package com.example.dell.projectapplication

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main4.*

class Main4Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main4)


        login2.setOnClickListener({

            var intent =Intent(this,StartActivity::class.java)
            startActivity(intent)
        })

        image.setOnClickListener({
            var intent =Intent(this,MainActivity::class.java)
            startActivity(intent)

        })
    }
}
