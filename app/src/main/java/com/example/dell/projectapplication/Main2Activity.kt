package com.example.dell.projectapplication

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main2.*

class Main2Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        var auth = FirebaseAuth.getInstance()

        cont.setOnClickListener({
            auth.createUserWithEmailAndPassword(editText1.text.toString(),editText2.text.toString())

            var intent =Intent(this ,Main3Activity::class.java)
            startActivity(intent)
        })

        imageV.setOnClickListener({
            var intent =Intent(this ,MainActivity::class.java)
            startActivity(intent)

        })



    }
}
