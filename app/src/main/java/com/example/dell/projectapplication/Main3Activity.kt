package com.example.dell.projectapplication

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main2.*
import kotlinx.android.synthetic.main.activity_main3.*

class Main3Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main3)


        signup2.setOnClickListener({

                var intent = Intent(this ,StartActivity::class.java)
                startActivity(intent)
        })

        image2.setOnClickListener({

            var intent = Intent(this ,MainActivity::class.java)
            startActivity(intent)
        })


    }
}
